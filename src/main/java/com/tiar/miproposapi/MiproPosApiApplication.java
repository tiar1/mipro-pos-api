package com.tiar.miproposapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MiproPosApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MiproPosApiApplication.class, args);
	}

}
